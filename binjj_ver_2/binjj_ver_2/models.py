# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'


class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'


class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'


class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'


class BinjDeal(models.Model):
    deal_id = models.IntegerField(primary_key=True)
    store = models.ForeignKey('BinjStore')
    ticket = models.ForeignKey('BinjTicket')
    template = models.ForeignKey('BinjTemplate')
    title = models.TextField()
    description = models.TextField()
    deal_image_url = models.CharField(max_length=255)
    redemption_limit = models.IntegerField(blank=True, null=True)
    posted_on = models.DateTimeField()
    posted_by = models.ForeignKey('BinjUser', db_column='posted_by')
    valid_days = models.CharField(max_length=255)
    validity_start_time = models.TimeField()
    validity_end_time = models.TimeField()
    expiry_date = models.DateTimeField()
    is_recurring = models.IntegerField()
    is_inactive = models.IntegerField()
    is_deleted = models.IntegerField()
    is_advertisement = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_deal'


class BinjDealUserAction(models.Model):
    deal_user_action_id = models.IntegerField(primary_key=True)
    deal_id = models.IntegerField()
    user_id = models.IntegerField()
    store_id = models.IntegerField(blank=True, null=True)
    action = models.CharField(max_length=6)
    is_deleted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_deal_user_action'


class BinjDevice(models.Model):
    device_id = models.IntegerField(primary_key=True)
    push_token = models.CharField(max_length=255)
    sns_endpoint_arn = models.TextField(blank=True)
    device_type = models.CharField(max_length=7)
    app_id = models.CharField(max_length=255)
    app_type = models.CharField(max_length=8)
    registered_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_device'


class BinjHub(models.Model):
    hub_id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    latitude = models.CharField(max_length=255)
    longitude = models.CharField(max_length=255)
    is_deleted = models.IntegerField(blank=True, null=True)
    added_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_hub'


class BinjLogin(models.Model):
    login_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField()
    device_token = models.CharField(max_length=255)
    login_time = models.DateTimeField()
    logout_time = models.DateTimeField()
    last_access_time = models.DateTimeField()
    session_token = models.CharField(max_length=255)
    session_expire_time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_login'


class BinjMerchant(models.Model):
    merchant_id = models.IntegerField(primary_key=True)
    merchant_name = models.CharField(max_length=255)
    owner_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    address = models.TextField()
    logo_url = models.CharField(max_length=255)
    added_on = models.DateTimeField()
    is_deleted = models.IntegerField()
    is_inactive = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_merchant'


class BinjMerchantUserMap(models.Model):
    merchant_user_map = models.IntegerField(primary_key=True)
    merchant = models.ForeignKey(BinjMerchant)
    user = models.ForeignKey('BinjUser')

    class Meta:
        managed = False
        db_table = 'binj_merchant_user_map'


class BinjNotificationLogs(models.Model):
    notification_id = models.IntegerField(primary_key=True)
    deal_id = models.IntegerField()
    user_id = models.IntegerField()
    store_id = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=47)
    notification_entity = models.CharField(max_length=255)
    description = models.TextField()
    added_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_notification_logs'


class BinjPermission(models.Model):
    permission_id = models.IntegerField(primary_key=True)
    resource = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'binj_permission'


class BinjPhoneRegisterLog(models.Model):
    phone_register_log_id = models.IntegerField(primary_key=True)
    phone = models.CharField(max_length=255)
    verification_code = models.IntegerField()
    is_verified = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_phone_register_log'


class BinjPushNotifications(models.Model):
    push_notification_id = models.IntegerField(primary_key=True)
    user_id = models.IntegerField()
    deal_id = models.IntegerField()
    is_redemeption_limit_notification_sent = models.IntegerField()
    is_expired_deal_notification_sent = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_push_notifications'


class BinjReview(models.Model):
    review_id = models.IntegerField(primary_key=True)
    user = models.ForeignKey('BinjUser')
    store = models.ForeignKey('BinjStore')
    comment = models.TextField()
    rating = models.IntegerField()
    added_on = models.DateTimeField()
    is_deleted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_review'


class BinjRole(models.Model):
    role_id = models.IntegerField(primary_key=True)
    role_name = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'binj_role'


class BinjRolePermissionMap(models.Model):
    role_permission_map_id = models.IntegerField()
    role = models.ForeignKey(BinjRole)
    permission = models.ForeignKey(BinjPermission)

    class Meta:
        managed = False
        db_table = 'binj_role_permission_map'


class BinjStore(models.Model):
    store_id = models.IntegerField(primary_key=True)
    hub_id = models.IntegerField()
    merchant_id = models.IntegerField()
    store_name = models.CharField(max_length=255)
    store_alias = models.CharField(max_length=10)
    logo_url = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)
    address = models.TextField()
    working_hours_from = models.TimeField()
    working_hours_to = models.TimeField()
    pass_key = models.CharField(max_length=255)
    is_deleted = models.IntegerField()
    is_inactive = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_store'


class BinjStoreImages(models.Model):
    image_id = models.IntegerField(primary_key=True)
    store = models.ForeignKey(BinjStore)
    image_url = models.CharField(max_length=255)
    added_by = models.IntegerField()
    added_on = models.DateTimeField()
    is_deleted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_store_images'


class BinjStoreUserMap(models.Model):
    store_user_map_id = models.IntegerField(primary_key=True)
    store = models.ForeignKey(BinjStore)
    user = models.ForeignKey('BinjUser')

    class Meta:
        managed = False
        db_table = 'binj_store_user_map'


class BinjSubscription(models.Model):
    subscription_id = models.IntegerField(primary_key=True)
    store = models.ForeignKey(BinjStore)
    subscriber_id = models.CharField(max_length=255)
    subscription_to = models.DateTimeField()
    subscription_from = models.DateTimeField()
    added_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_subscription'


class BinjTemplate(models.Model):
    template_id = models.IntegerField(primary_key=True)
    logo_position = models.IntegerField()
    merchant_name_position = models.IntegerField()
    merchant_name_color = models.CharField(max_length=255)
    address_position = models.IntegerField()
    address_color = models.CharField(max_length=255)
    validity_position = models.IntegerField()
    validity_color = models.CharField(max_length=255)
    overlay_image_url = models.CharField(max_length=255)
    template_html = models.TextField()
    is_deleted = models.IntegerField()
    added_on = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'binj_template'


class BinjTicket(models.Model):
    ticket_id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=255)
    added_on = models.DateTimeField()
    is_deleted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_ticket'


class BinjUser(models.Model):
    user_id = models.IntegerField(primary_key=True)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    firstname = models.CharField(max_length=255, blank=True)
    lastname = models.CharField(max_length=255, blank=True)
    age = models.IntegerField(blank=True, null=True)
    gender = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    user_logo_url = models.CharField(max_length=255, blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=255, blank=True)
    pin_code = models.CharField(max_length=255, blank=True)
    added_on = models.DateTimeField()
    modified_on = models.DateTimeField()
    is_inactive = models.IntegerField()
    is_deleted = models.IntegerField()
    is_notification_setting_off = models.IntegerField()
    is_location_service_setting_off = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'binj_user'


class BinjUserRoleMap(models.Model):
    user_role_map_id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(BinjUser)
    role = models.ForeignKey(BinjRole)

    class Meta:
        managed = False
        db_table = 'binj_user_role_map'


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.IntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'
