from django.conf.urls import patterns, include, url
from django.contrib import admin
# 04/16/2015 imports for REST API
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets
from django.conf.urls.static import static
from . import settings
# 04/16/2015 Code for REST API
# Serializers define the API representation
class UserSerializer(serializers. HyperlinkedModelSerializer):
	class Meta:
		model = User
		fields = ('url', 'username', 'email', 'is_staff')
		
# ViewSets define the view behavior
class UserViewSet(viewsets.ModelViewSet):
	queryset = User.objects.all()
	serializer_class = UserSerializer

# Routers provide an easy way of automatically determining the URL conf
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

# Wire up our API using the automatic URL routing
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'binjj_ver_2.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # REST framework added on 04/16/2015
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
)+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
