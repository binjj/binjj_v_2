from django.contrib import admin
from . import models
from .models import BinjHub
from .models import BinjDeal
from .models import BinjStore
from .models import BinjMerchant
from .models import BinjUser
from .models import BinjMerchantUserMap

from django import forms
from django.forms import widgets
import datetime

# Register your models here.
# To display stores under a particular merchant
class StoreInLineForMerchant(admin.TabularInline):
    model = BinjStore
    fk_name = 'merchant_id'
    raw_id_field = 'hub_iud'
    exclude = ['store_id', 'logo_url', 'address', 'working_hours_from', 'working_hours_to',
                'is_deleted', 'is_inactive']
    extra = 0
    verbose_name = "Stores"
    show_change_link = False

class StoreInLineForHub(admin.TabularInline):
    model = BinjStore
    fk_name = 'hub_id'
    exclude = ['store_id', 'logo_url', 'address', 'working_hours_from', 'working_hours_to',
                'is_deleted', 'is_inactive']
    extra = 0
    verbose_name = "Stores"
    show_change_link = True

class BinjHubAdmin(admin.ModelAdmin):
    #fields = ['name', 'city', 'latitude', 'longitude', 'added_on']
    fieldsets = [
        ('Name & City', {'fields': [('name', 'city')]}),
        ('Geo Location', {'fields': [('latitude', 'longitude')]})
    ]
    list_display = ['name', 'city', 'latitude', 'longitude']
    inlines = [StoreInLineForHub,]
admin.site.register(BinjHub, BinjHubAdmin)

# Email is not a part of the Binj Merchant model, instead it is in BinjUser
# We will display an email entry field on Binj Merchant Add/Change
# but saving it in BinjUser
class EmailEntryField(forms.ModelForm):
    email_field = forms.EmailField(label='Enter new email')

    class Meta:
        model = BinjMerchant
        fields = ['email_field',]

class BinjMerchantAdmin(admin.ModelAdmin):
    form = EmailEntryField

    def get_user_email(self, obj):
        # Merchant is actually a user and hence his/her email is
        # stored in the BinjUser table. The mapping between Merchant and
        # corresponding user id is implemented via BinjMerchantUserMap table
        return (BinjMerchantUserMap.objects.get(merchant=obj.merchant_id).user).email

    def save_model(self, request, obj, form, change):
        # Get the mapping object for this merchant
        bmum = BinjMerchantUserMap.objects.get(merchant=obj.merchant_id)
        # Now, get the user object for this merchant
        bu = BinjUser.objects.get(user_id = bmum.user.user_id)
        if form.is_valid():
            bu.email = form.cleaned_data['email_field']
        bu.save()
        obj.save()

    get_user_email.short_description = 'email'
    inlines = [StoreInLineForMerchant,]
    list_display = ['merchant_name', 'owner_name', 'phone', 'get_user_email', 'image_tag']
    ordering = ['merchant_name']
    readonly_fields = ['get_user_email', 'image_tag']

    fieldsets = [
        (None, {'fields': [('merchant_name', 'owner_name')]}),
        (None, {'fields': [('logo_url', 'image_tag')]}),
        (None, {'fields': [('address', 'phone', 'get_user_email', 'email_field')]}),
        (None, {'fields': [('added_on', 'is_deleted', 'is_inactive')]})
    ]
#admin.site.unregister(BinjMerchant)
admin.site.register(BinjMerchant, BinjMerchantAdmin)

class DealInline(admin.TabularInline):
    model = BinjDeal
    exclude = ['deal_id', 'ticket', 'template', 'deal_image_url', 'valid_days',
               'is_recurring', 'is_advertisement', 'posted_by']
    readonly_fields = ['title', 'description', 'redemption_limit', 'validity_start_time', 'validity_end_time',
                        'expiry_date', 'is_inactive', 'is_deleted',]
    extra = 0
    verbose_name = "Deals under this store"
    show_change_link = True

class BinjStorelAdmin(admin.ModelAdmin):
    def get_merchant(self, obj):
        return obj.merchant_id.merchant_name
    get_merchant.short_description = 'Merchant'
    def get_hub(self, obj):
        return obj.hub_id.name
    get_hub.short_description = 'Hub'
    list_display = ['store_name', 'store_alias', 'get_merchant',  'get_hub', 'pass_key']
    ordering = ['store_name']
    readonly_fields = ['image_tag']
    fieldsets = [
        ('Hub & Merchant', {'fields': [('hub_id', 'merchant_id')]}),
        ('Name  & Alas', {'fields': [('store_name', 'store_alias')]}),
        ('Address & Contact', {'fields': [('address', 'phone')]}),
        ('Other Details', {'fields': [('working_hours_from', 'working_hours_to', 'pass_key'), ('is_deleted', 'is_inactive')]})
    ]
    inlines = [DealInline,]
admin.site.register(BinjStore, BinjStorelAdmin)

def mark_deleted(modeladmin, request, queryset):
    rows_updated = queryset.update(is_deleted=True)
    message = "%s Row(s) were marked as deleted" % rows_updated
    modeladmin.message_user(request, message)
mark_deleted.short_description = "Mark selected Row(s) as Deleted"

def mark_undeleted(modeladmin, request, queryset):
    rows_updated = queryset.update(is_deleted=False)
    message = "%s Row(s) were marked as undeleted" % rows_updated
    modeladmin.message_user(request, message)
mark_undeleted.short_description = "Mark selected Row(s) as UnDeleted"

def mark_active(modeladmin, request, queryset):
    rows_updated = queryset.update(is_inactive=True)
    message = "%s row(s) were marked as Active" % rows_updated
    modeladmin.message_user(request, message)
mark_active.short_description = "Mark selected row(s) as Active"

def mark_inactive(modeladmin, request, queryset):
    rows_updated = queryset.update(is_inactive=False)
    message = "%s row(s) were marked as Inactive" % rows_updated
    modeladmin.message_user(request, message)
mark_inactive.short_description = "Mark selected row(s) as Inactive"

def repost_deal(modeladmin, request, queryset):
    date_today = datetime.date.today()
    midnight = "23:59:59"
    queryset.update(expiry_date=date_today)
    rows_updated = queryset.update(validity_end_time=midnight)
    message = "%s deals have now been reposted" % rows_updated
    modeladmin.message_user(request, message)
repost_deal.short_description = "Repost selected Deals"

class BinjDealAdmin(admin.ModelAdmin):
    def get_merchant(self, obj):
         return BinjStore.objects.get(store_id=obj.store_id).merchant_id
    get_merchant.short_description = "Merchant"
    list_display = ['title', 'get_merchant', 'store',  'posted_on', 'validity_start_time', 'validity_end_time', 'expiry_date',
                    'is_deleted', 'is_inactive', 'image_tag']
    readonly_fields = ['image_tag']
    fieldsets = [
        ('Store, Title, Image & Description', {'fields': ['store', ('title', 'description'),'image_tag', 'deal_image_url', ]}),
        ('Expired/Inactive', {'fields': [('is_inactive', 'is_deleted'), ]}),
        (
            'Validity Details',
            {'fields': [('posted_on', 'validity_start_time', 'validity_end_time'), ('expiry_date')]}),
    ]
    #list_filter = ['posted_on', 'expiry_date', 'is_deleted', 'is_inactive']

    list_editable = ['expiry_date', 'validity_start_time', 'validity_end_time']
    search_fields = ['store__store_alias', 'store__merchant_id__merchant_name']

    actions = [mark_deleted, mark_undeleted, mark_active, mark_inactive, repost_deal]

admin.site.register(BinjDeal, BinjDealAdmin)

class BinjUserAdmin(admin.ModelAdmin):
    list_display = ('firstname', 'lastname', 'email')
admin.site.register(BinjUser, BinjUserAdmin)




